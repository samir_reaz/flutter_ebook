import 'package:ebook/consttant.dart';
import 'package:ebook/screens/details_screen.dart';
import 'package:ebook/widgets/best_Of_TheDay_Card.dart';
import 'package:ebook/widgets/book_rating.dart';
import 'package:ebook/widgets/reading_list_card.dart';
import 'package:ebook/widgets/two_side_round_button.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      body: Scaffold(
        body: Container(
          width: double.infinity,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('images/main_page_bg.png'),
              alignment: Alignment.topCenter,
              fit: BoxFit.fitWidth,
            ),
          ),
          child: SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(height: size.height * .1),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 24.0),
                  child: RichText(
                    text: TextSpan(
                      style: Theme.of(context).textTheme.display1,
                      children: [
                        TextSpan(text: 'What are you\n reading'),
                        TextSpan(
                          text: ' today?',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(height: 30.0),
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    children: <Widget>[
                      ReadingListCare('images/book-1.png',
                          'Crushing & Influence', 'Gary Venchuk', 4.9, () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) {
                              return DetailsScreen();
                            },
                          ),
                        );
                      }, null),
                      ReadingListCare(
                          'images/book-2.png',
                          'Top Ten Business Hacks',
                          'Hernam Joel',
                          4.8,
                          null,
                          null),
                      SizedBox(
                        width: 30,
                      )
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 24),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      RichText(
                        text: TextSpan(
                          style: Theme.of(context).textTheme.display1,
                          children: [
                            TextSpan(text: 'Best of The '),
                            TextSpan(
                                text: 'Day',
                                style: TextStyle(fontWeight: FontWeight.bold)),
                          ],
                        ),
                      ),
                      BestOfTheDayCard(),
                      RichText(
                        text: TextSpan(
                            style: Theme.of(context).textTheme.display1,
                            children: [
                              TextSpan(text: 'Contune '),
                              TextSpan(
                                  text: 'reading...',
                                  style: TextStyle(fontWeight: FontWeight.bold))
                            ]),
                      ),
                      SizedBox(height: 20),
                      Container(
                        height: 80,
                        width: double.infinity,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(38.5),
                          boxShadow: [
                            BoxShadow(
                              offset: Offset(0, 10),
                              blurRadius: 33,
                              color: Color(0xFFd3d3dd).withOpacity(.84),
                            ),
                          ],
                        ),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(38.5),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Expanded(
                                child: Padding(
                                  padding: EdgeInsets.only(left: 30, right: 20),
                                  child: Row(
                                    children: <Widget>[
                                      Expanded(
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.end,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Text(
                                              'Crushing & Influence',
                                              style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                              ),
                                            ),
                                            Text(
                                              'Gray Venchuk',
                                              style: TextStyle(
                                                  color: kLightBlackColor),
                                            ),
                                            Align(
                                              alignment: Alignment.bottomRight,
                                              child: Text(
                                                'Chapter 7 of 10',
                                                style: TextStyle(
                                                  fontSize: 10,
                                                  color: kLightBlackColor,
                                                ),
                                              ),
                                            ),
                                            SizedBox(
                                              height: 5,
                                            )
                                          ],
                                        ),
                                      ),
                                      Image.asset(
                                        'images/book-1.png',
                                        width: 55,
                                      )
                                    ],
                                  ),
                                ),
                              ),
                              Container(
                                height: 7,
                                width: size.width * .65,
                                decoration: BoxDecoration(
                                  color: kProgressIndicator,
                                  borderRadius: BorderRadius.circular(7),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                      SizedBox(height: 40)
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
