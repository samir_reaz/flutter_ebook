import 'package:flutter/material.dart';

import '../consttant.dart';

class TwoSideRoundButton extends StatelessWidget {
  final String text;
  final double radious = 29.0;
  final Function press;

  TwoSideRoundButton(this.text, this.press);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Container(
        alignment: Alignment.center,
        padding: EdgeInsets.symmetric(vertical: 10.0),
        decoration: BoxDecoration(
            color: kBlackColor,
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(radious),
                bottomRight: Radius.circular(radious))),
        child: Text(
          text,
          style: TextStyle(color: Colors.white),
        ),
      ),
    );
  }
}
