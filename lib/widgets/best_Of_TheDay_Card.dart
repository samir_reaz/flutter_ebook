import 'package:ebook/widgets/two_side_round_button.dart';
import 'package:flutter/material.dart';

import '../consttant.dart';
import 'book_rating.dart';

class BestOfTheDayCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.symmetric(vertical: 20),
      width: double.infinity,
      height: 205,
      child: Stack(
        children: <Widget>[
          Positioned(
            bottom: 0,
            left: 0,
            right: 0,
            child: Container(
              padding:
                  EdgeInsets.only(left: 24, top: 24, right: size.width * .35),
              height: 185,
              width: double.infinity,
              decoration: BoxDecoration(
                color: Color(0xffEFEFEF).withOpacity(.45),
                borderRadius: BorderRadius.circular(29),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    'New York Time Best For 11th March 2020',
                    style: TextStyle(
                      fontSize: 9,
                      color: kLightBlackColor,
                    ),
                  ),
                  SizedBox(height: 5),
                  Text(
                    'How To WIn \nFriends & Influence',
                    style: Theme.of(context).textTheme.title,
                  ),
                  Text(
                    'Gray Venchuk',
                    style: TextStyle(color: kBlackColor),
                  ),
                  SizedBox(height: 10),
                  Row(
                    children: <Widget>[
                      BookRating(4.9),
                      SizedBox(width: 10),
                      Expanded(
                          child: Text(
                        'In publishing and graphic design, Lorem ipsum is a placeholder text commonly used to demonstrate the visual form of a document or a typeface',
                        maxLines: 3,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(fontSize: 10, color: kBlackColor),
                      ))
                    ],
                  ),
                ],
              ),
            ),
          ),
          Positioned(
            right: 0,
            top: 0,
            child: Container(
              child: Image.asset(
                'images/book-3.png',
                width: size.width * .37,
              ),
            ),
          ),
          Positioned(
              right: 0,
              bottom: 0,
              child: SizedBox(
                height: 40,
                width: size.width * .35,
                child: TwoSideRoundButton('Read', null),
              ))
        ],
      ),
    );
  }
}
