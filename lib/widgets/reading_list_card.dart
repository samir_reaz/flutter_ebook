import 'package:ebook/widgets/two_side_round_button.dart';
import 'package:flutter/material.dart';

import '../consttant.dart';
import 'book_rating.dart';

class ReadingListCare extends StatelessWidget {
  final String image;
  final String title;
  final String auth;
  final double rating;
  final Function pressDetails;
  final Function pressRead;

  ReadingListCare(this.image, this.title, this.auth, this.rating,
      this.pressDetails, this.pressRead);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 24, bottom: 40.0),
      height: 245,
      width: 202,
      child: Stack(
        children: <Widget>[
          Positioned(
            left: 0,
            right: 10,
            bottom: 0,
            child: Container(
              height: 221,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(30.0),
                boxShadow: [
                  BoxShadow(
                    offset: Offset(0.0, 10.0),
                    blurRadius: 33,
                    color: kShadowColor,
                  )
                ],
              ),
            ),
          ),
          Image.asset(
            image,
            width: 150.0,
          ),
          Positioned(
            top: 35,
            right: 10,
            child: Column(
              children: <Widget>[
                IconButton(icon: Icon(Icons.favorite_border), onPressed: () {}),
                BookRating(rating),
              ],
            ),
          ),
          Positioned(
              top: 160,
              child: Container(
                height: 85,
                width: 190,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(left: 24),
                      child: RichText(
                        maxLines: 2,
                        text: TextSpan(
                            style: TextStyle(color: kBlackColor),
                            children: [
                              TextSpan(
                                text: '$title\n',
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              TextSpan(
                                text: auth,
                                style: TextStyle(color: kLightBlackColor),
                              )
                            ]),
                      ),
                    ),
                    Spacer(),
                    Row(
                      children: <Widget>[
                        GestureDetector(
                          onTap: pressDetails,
                          child: Container(
                            width: 101,
                            padding: EdgeInsets.symmetric(vertical: 10),
                            alignment: Alignment.center,
                            child: Text('Details'),
                          ),
                        ),
                        Expanded(
                          child: TwoSideRoundButton(
                            'Read',
                            pressRead,
                          ),
                        )
                      ],
                    )
                  ],
                ),
              ))
        ],
      ),
    );
  }
}
